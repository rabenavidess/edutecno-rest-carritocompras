package dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class OrdenDespacho{

	private String direccion;
	private String nombre;
	private List<Articulo> articulo = new ArrayList<Articulo>();
	private double monto;
	private Date fecha;
	public OrdenDespacho(String direccion, String nombre, List<Articulo> articulo, double monto, Date fecha) {
		super();
		this.direccion = direccion;
		this.nombre = nombre;
		this.articulo = articulo;
		this.monto = monto;
		this.fecha = fecha;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<Articulo> getArticulo() {
		return articulo;
	}
	public void setArticulo(List<Articulo> articulo) {
		this.articulo = articulo;
	}
	public double getMonto() {
		return monto;
	}
	public void setMonto(double monto) {
		this.monto = monto;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	
}
