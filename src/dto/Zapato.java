package dto;

public class Zapato extends Articulo {


	private String modelo;

	public Zapato(double precio, String nombre, String codigo, Talla talla, String marca, String modelo) {
		super(precio, nombre, codigo, talla, marca);
		this.modelo = modelo;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	public String toString() {
		return "Zapato [modelo = "+ modelo + 
						" talla = "+talla+
						" marca = "+marca+
						" precio = "+precio+
						" nombre = "+nombre+
						" codigo = "+codigo +"]";
	}
	
}
