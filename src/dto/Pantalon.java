package dto;

public class Pantalon extends Articulo {
	private String color;
	private int bolsillo;
	public Pantalon(double precio, String nombre, String codigo, Talla talla, String marca, String color,
			int bolsillo) {
		super(precio, nombre, codigo, talla, marca);
		this.color = color;
		this.bolsillo = bolsillo;
	}
	public String getColor() {
		return color;
	}
	public void setColor(String color) {
		this.color = color;
	}
	public int getBolsillo() {
		return bolsillo;
	}
	public void setBolsillo(int bolsillo) {
		this.bolsillo = bolsillo;
	}
	public String toString() {
		return "Zapato [color = "+ color +
						" bolsillo = " + bolsillo+
						" talla = "+talla+
						" marca = "+marca+
						" precio = "+precio+
						" nombre = "+nombre+
						" codigo = "+codigo +"]";
	}
}
