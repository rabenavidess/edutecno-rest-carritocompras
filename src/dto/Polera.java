package dto;


public class Polera extends Articulo{
	private String color;

	public Polera(double precio, String nombre, String codigo, Talla talla, String marca, String color) {
		super(precio, nombre, codigo, talla, marca);
		this.color = color;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}
	public String toString() {
		return "Polera [color = "+ color + 
						" talla = "+talla+
						" marca = "+marca+
						" precio = "+precio+
						" nombre = "+nombre+
						" codigo = "+codigo +"]";
	}
}
	