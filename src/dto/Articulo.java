package dto;

public abstract class Articulo {

	protected double precio;
	protected String nombre;
	public String codigo;
	protected Talla talla;
	protected String marca;
	
	public Articulo(double precio, String nombre, String codigo, Talla talla, String marca) {
		super();
		this.precio = precio;
		this.nombre = nombre;
		this.codigo = codigo;
		this.talla = talla;
		this.marca = marca;
	}


	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public Talla getTalla() {
		return talla;
	}

	public void setTalla(Talla talla) {
		this.talla = talla;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}
	
}
