package Controller;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Scanner;

import dto.Articulo;
import dto.OrdenDespacho;

public class MetodoImp implements Metodo {

	Scanner src = new Scanner(System.in);

	@Override
	public boolean agregarProducto() {

		System.out.println("Ingrese Y para Si o N para No");
		String confirmacion = src.next();

		if (confirmacion.toUpperCase().equals("Y")) {
			return true;
		}

		return false;
	}

	@Override
	public boolean pagarDebito(double valor) {

		System.out.println("�Est� seguro que desea pagar con d�bito el total de?");
		System.out.println("Ingrese Y para si o N para no");
		String confirmacion = src.next();

		if (confirmacion.toUpperCase().equals("Y")) {
			generarOrden();

			return true;
		}
		return false;
	}

	@Override
	public boolean pagarCredito(double valor) {

		System.out.println("�Est� seguro que desea pagar con d�bito el total de " + valor + "?");
		System.out.println("Ingrese Y para si o N para no");
		String confirmacion = src.next();

		if (confirmacion.toUpperCase().equals("Y")) {
			System.out.println("Cuantas cuotas desea pagar? (1-24)");
			int cuota = src.nextInt();
			System.out.println("Esta seguro que desea pagar con " + cuota + " cuotas?");
			System.out.println("Ingrese Y para si o N para no");
			String confirmacion1 = src.next();
			if (confirmacion1.toUpperCase().equals("Y")) {
				generarOrden();
				return true;
			}

		}

		return false;
	}

	@Override
	public boolean escogerMenu(int op, double valor) {
		if (op == 1) {
			pagarCredito(valor);
			return true;
		}
		if (op == 2) {
			pagarDebito(valor);
			return true;
		}
		return false;
	}

	@Override
	public void generarOrden() {
		String direccion;
		String nombre;
		List<Articulo> articulo = null;
		double monto = 0;
		Date fecha = new Date();

		System.out.println("Cual es la direccion de despacho?");
		direccion = src.next();
		System.out.println("Cual es el nombre de quien recibe el producto?");
		nombre = src.next();

		OrdenDespacho OrdenDespacho = new OrdenDespacho(direccion, nombre, articulo, monto, fecha);
		System.out.println("Orden de despacho generada correctamente");
	}
}
