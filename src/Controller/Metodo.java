package Controller;

public interface Metodo {
	
	public boolean agregarProducto();

	boolean pagarDebito(double valor);
	
	boolean pagarCredito(double valor);
	
	public boolean escogerMenu(int op, double valor);
	
	public void generarOrden();
	
}
